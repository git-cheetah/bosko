#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#define GIT_CHEETAH_REG_PATH "SOFTWARE\\Git-Cheetah"
#define GIT_CHEETAH_REG_PATHTOMSYS "PathToMsys"

TCHAR * msys_path(void);

#endif /* SYSTEMINFO_H */
